# About TechNCo Project

Bu proje [Visions Nouvelles](https://www.visionsnouvelles.com/ "Visions Nouvelles") firması tarafından [PrestaShop](https://www.prestashop.com/ "PrestaShop") e-ticaret altyapısı kullanılarak geliştirilmektedir.

[![](https://www.visionsnouvelles.com/wp-content/uploads/2020/07/Visions-Web-logo-1.png)](https://www.visionsnouvelles.com/ "Visions Nouvelles")

# Gereksinimler
**ÖNEMLİ BİLGİLENDİRME:** Kurulum talimatlarına geçmeden önce; 

Tema klasöründe yer alan **[README.md](./themes/TechncoTheme/README.md#gereksinimler "README.md")** dosyasında belirtilen **minimum gereksinimleri** sağladığınızdan emin olunuz.

Aşağıda belirtilen PrestaShop versiyonu ve sistem özelliklerine sahip bir sunucuda kurulum başarıyla test edilmiştir. 
> Bu sistem özellikleri referans alabilmeniz açısından açıkça belirtilmiştir.

- **PrestaShop:** PrestaShop 1.7.7.4 ([Stable](https://www.prestashop.com/en/versions "Stable"))
- **Web Sunucusu:** Apache 2.4.47
- **PHP:** PHP 7.3.28
- **MySQL:** MariaDB 10.4.18

Farklı yapılandırma özelliklerine sahip bir sunucuya kurulum gerçekleştiriyor ve hata alıyorsanız; 
<br>
**Lütfen aldığınız hatayı ve sunucu yapılandırmanızı açıkça belirten **[Issue](https://gitlab.com/visions-nouvelles-iy/web/ps_technco/-/issues "Issues")** geri bildirimi gönderin.**

## Kurulum Talimatları

> Not: Bu kurulum için [Git](https://git-scm.com/ "Git") ve [Composer](https://getcomposer.org/ "Composer") gereklidir.

### Installation

Komut istemcinizi (Terminal) açın, ardından:

1. Sunucu ana dizinine gidin

	`> cd /htdocs`

2. Bu repoyu klonlayın

	`> git clone https://gitlab.com/visions-nouvelles-iy/web/ps_technco.git`

3. **ps_technco** klasörüne geçiş yapın

	`> cd ps_technco`

4. Bağımlılıkları yükleyin

	 `> composer install`

### Using MySQL
+ **Simple Method**: İnternet Tarayıcısı kullanarak:
	1. Sunucunuzun PhpMyAdmin sayfasına gidin
	Örneğin: http://localhost/phpMyAdmin
	
	2. **ps_technco** adında bir veritabanı tablosu oluşturunuz (utf8mb4_general_ci)
	
	3. Ardından **[ps_technco.sql](./_avol/ps_technco.sql "ps_technco.sql")** dosyasını içe aktarınız.

+ **Advanced Method**: Komut istemcisi Terminal kullanarak (Önerilir):
	> Not: Terminal üzerinden MySQL ile bağlantı kurabilmek için öncelikle mysql klasör konumunu bulmalısınız. 
	<br>Benim konumum: **~~e:\xampp\mysql\bin~~** (sizinki farklı bir konumda veya uzak sunucuda olabilir)
	<br>Eğer Xampp kullanıyorsanız **c:\xammp\mysql\bin** yolunu kontrol ediniz.
	
	Komut istemcinizi (Terminal) açın, ardından:
	
	1. **mysql\bin** konumuna gidiniz
	
		`> cd e:\xampp\mysql\bin`

	2. MySQL ile bağlantı kurunuz

	 	`> mysql -u root -p`

	3. MySQL şifrenizi girin

	 	Terminal sizden MySQL şifrenizi isteyecek (eğer şifre yoksa boş bırakınız)

	4. **ps_technco** adında bir veritabanı tablosu oluşturunuz

		> NOT: Daha önce **ps_technco** adında bir veritabanı tablosu oluşturduysanız tabloyu kaldırmayı unutmayınız: `DROP DATABASE ps_technco;`
	
		`> CREATE DATABASE ps_technco COLLATE utf8mb4_general_ci;`


	5. **ps_technco** veritabanına geçiş yapın

		`> use ps_technco;`

	6. Ardından **[ps_technco.sql](./_avol/ps_technco.sql "ps_technco.sql")** dosyasını terminal komutu ile içe aktarınız

		`> source \htdocs\ps_technco\_avol\ps_technco.sql`

	7. İçe aktarma işlemi tamamlandığında çıkış yapın.

		`> exit`

## Done :fire:

http://localhost/ps_technco

http://localhost/ps_technco/admin-dev

**Email**: <br>`admin@localhost.net`

**Şifre**: <br>adminpass123
